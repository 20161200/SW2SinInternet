﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileAR : MonoBehaviour
{
    [SerializeField] private float speed = 20f;
    public float lifeTime;

    private bool permitidoDer = true;

    public LayerMask whatIsSolid;
    public float distance;
    public int damage;

    GameObject PLAYER;
    GunSpawner gunSpawner;
    PlayerMovement playerMovement;

    
    private void Start()
    {
        PLAYER = GameObject.Find("GunSpawner").GetComponent<GunSpawner>().player;
        playerMovement = PLAYER.GetComponent<PlayerMovement>();

        Invoke("DestroyProjectile", lifeTime);


        if (playerMovement.orientation == true)
        {
            permitidoDer = true;
        }
        else
        {
            permitidoDer = false;
        }
    }

    //as
    private void Update()
    {

        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, transform.up, distance, whatIsSolid);
        if (hitInfo.collider != null)
        {
            if (hitInfo.collider.CompareTag("Enemy"))
            {
                hitInfo.collider.GetComponent<Enemy>().TakeDamage(damage);
            }
            DestroyProjectile();
        }

        if (permitidoDer == true)
        {
            transform.Translate(Vector2.right * speed * Time.deltaTime);
        }
        else
        {
            transform.Translate(Vector2.left * speed * Time.deltaTime);
        }
    }
    
    void DestroyProjectile()
    {
        Destroy(gameObject);
    }
}
