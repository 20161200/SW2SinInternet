﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAR : MonoBehaviour
{
    public GameObject projectile;

    public Transform shotPoint;

    public bool orientation;

    private KeyCode shoot;
    private KeyCode left;
    private KeyCode right;

    private void Start()
    {
        if(transform.parent.name == "Player")
        {
            shoot = KeyCode.V;
            left = GameObject.Find("Player").GetComponent<PlayerMovement>().left;
            left = GameObject.Find("Player").GetComponent<PlayerMovement>().right;
        }
        else if(transform.parent.name == "Player (1) Variant")
        {
            shoot = KeyCode.Space;
            left = GameObject.Find("Player (1) Variant").GetComponent<PlayerMovement>().left;
            left = GameObject.Find("Player (1) Variant").GetComponent<PlayerMovement>().right;
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(shoot))
        {           
            InstantiateProjectileAR();
        }

        if (Input.GetKeyDown(left))
        {
            orientation = false;
        }
        else if (Input.GetKeyDown(right))
        {
            orientation = true;
        }
    }

    
    public void InstantiateProjectileAR()
    {
        Instantiate(projectile, shotPoint.position, transform.rotation);
    }

}
