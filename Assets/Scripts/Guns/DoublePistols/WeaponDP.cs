﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponDP : MonoBehaviour
{
    public GameObject projectileDer;   //Prefab projectileDer
    public GameObject projectileIzq;   //Prefab projectileIzq

    public Transform shotPointDer;     //Boca de fuego de la pistola derecha
    public Transform shotPointIzq;     //Boca de fuego de la pistola izquierda

    private KeyCode shoot;

    private void Start()
    {
        if (transform.parent.name == "Player")
        {
            shoot = KeyCode.V;
        }
        else if (transform.parent.name == "Player (1) Variant")
        {
            shoot = KeyCode.Space;
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(shoot))
        {
            Instantiate(projectileDer, shotPointDer.position, transform.rotation);
            Instantiate(projectileIzq, shotPointIzq.position, transform.rotation);
        }
    }
}
