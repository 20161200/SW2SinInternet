﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesMovement : MonoBehaviour
{
    public float runSpeed = 50f;
    private int orientation;

    public CharacterController2D controller;

    private void Start()
    {
        orientation = Random.Range(0, 2) * 2 - 1; // -1 o 1
    }

    private void FixedUpdate()
    {
        controller.Move(orientation * runSpeed * Time.fixedDeltaTime, false, false);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            orientation = orientation * -1;
        }
    }
}
