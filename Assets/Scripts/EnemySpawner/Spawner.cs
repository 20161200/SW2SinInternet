﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] enemies; //Aquí va todos los prefabs que se quieren spawnear
    Vector2 whereToSpawn;
    int enemy;

    void Start()
    {
        InvokeRepeating("Spawn", 1.5f, 1.5f);
    }

    public void Spawn()
    {
        whereToSpawn = new Vector2(transform.position.x, transform.position.y);
        enemy = Random.Range(0, enemies.Length);
        Instantiate(enemies[enemy], whereToSpawn, Quaternion.identity);
    }
}
