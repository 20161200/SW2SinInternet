﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GunSpawner : MonoBehaviour
{
    public GameObject[] armas;
    public GameObject player;

    private float[] posY = { 1f, -1.8f, -4f, 3.3f };

    private bool tieneArma = false;

    public Text puntaje;
    private int valorPuntaje = 0;
    

    private void Start()
    {
        CambiarPosicionCaja();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            player = collision.gameObject;

            if (player.GetComponent<PlayerMovement>().tieneArma == false)
            {
                InstanciarArma();
                player.GetComponent<PlayerMovement>().tieneArma = true;
            }

            if (player.GetComponent<PlayerMovement>().tieneArma == true)
            {
                Destroy(player.GetComponent<Transform>().GetChild(2).gameObject);
                InstanciarArma();
            }

            valorPuntaje++;
            puntaje.text = valorPuntaje.ToString();

        }
    }


    public void InstanciarArma()
    {
        GameObject arma = Instantiate(armas[Random.Range(0, armas.Length)], transform.localPosition, transform.rotation);
        arma.transform.SetParent(player.transform);

        arma.transform.localPosition = new Vector3(0f, 0f, 0f);
        arma.transform.localScale = new Vector3(1f, 1f, 1f);

        CambiarPosicionCaja();
    }

    public void CambiarPosicionCaja()
    {
        float y = posY[Random.Range(0, posY.Length)];
        float x;

        switch (y)
        {
            case 1f:
                x = Random.Range(-6f, 6f) * (Random.Range(0, 2) * 2 - 1);
                gameObject.transform.position = new Vector3(x, y, 0f);
                break;

            case -1.8f:
                x = Random.Range(-6f, 6f);
                gameObject.transform.position = new Vector3(x, y, 0f);
                break;

            case -4f:
                x = Random.Range(-6f, 6f) * (Random.Range(0, 2) * 2 - 1);
                gameObject.transform.position = new Vector3(x, y, 0f);
                break;

            case 3.3f:
                x = Random.Range(-6f, 6f);
                gameObject.transform.position = new Vector3(x, y, 0f);
                break;
        }
    }
}
