﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour
{
    public GameObject mainPanel;
    public GameObject levelPanel;
    public GameObject levelPanel2;

    public void onClickJugar()
    {
        mainPanel.SetActive(false);
        levelPanel.SetActive(true);
    }

    public void onClickLevel1()
    {
        SceneManager.LoadScene("Level1");
    }
    public void onClickLevel2()
    {
        SceneManager.LoadScene("Level2");
    }
    public void onClickLevel3()
    {
        SceneManager.LoadScene("Level3");
    }

    public void onClickTwoPlayers()
    {
        mainPanel.SetActive(false);
        levelPanel2.SetActive(true);
    }

    public void onClickLevel1_2P()
    {
        SceneManager.LoadScene("Level1_2P");
    }
    public void onClickLevel2_2P()
    {
        SceneManager.LoadScene("Level2_2P");
    }
    public void onClickLevel3_2P()
    {
        SceneManager.LoadScene("Level3_2P");
    }
}
