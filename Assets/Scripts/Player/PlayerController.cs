﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Si el personaje choca con un objeto con el tag enemigo, entonces
        if (collision.gameObject.tag == "Enemy")
        {
            //El enemigo se destruye
            Destroy(collision.gameObject);

            //El jugador se destruye
            Destroy(gameObject);
            SceneManager.LoadScene("MainMenu");
        }


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Destroyer")
        {
            Destroy(gameObject);
            SceneManager.LoadScene("MainMenu");
        }
    }
}
