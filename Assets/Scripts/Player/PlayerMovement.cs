﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //Movimiento  
    public CharacterController2D controller;

    public float runSpeed = 60f;
    private float horizontalMove = 0f;
    private bool jump;
    public KeyCode left;
    public KeyCode right;
    public KeyCode jumpKey;

    public bool tieneArma = false;

    public bool orientation;



    private void Update()
    {

        if (Input.GetKeyDown(left))
        {
            horizontalMove = -runSpeed;
        }
        else if (Input.GetKeyDown(right))
        {
            horizontalMove = runSpeed;
        }
        else if (Input.GetKeyUp(left) || Input.GetKeyUp(right))
        {
            horizontalMove = 0;
        }


        if (Input.GetKeyDown(jumpKey))
        {
            jump = true;
        }
        
        if (Input.GetKeyDown(right)) { orientation = true; }
        else if (Input.GetKeyDown(left)) { orientation = false; }
    }

    private void FixedUpdate()
    {
        CheckInput();    
    }

    private void CheckInput()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, false, jump);
        jump = false;
    }

    
}
